<?php

namespace Drupal\vinasol_contest\Services;

use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
* Service to manage the contest prizes assignment.
*/
class ManagerService
{
    /**
     * Get the oldest published and unassigned prize.
     * string $country Country code
     * @return \Drupal\Core\Entity\EntityInterface|null
     */
    public function checkPrize(string $country)
    {
        $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');

        $results = $nodeStorage->getQuery()
            ->condition('type', 'prize')
            ->condition('status', Node::PUBLISHED)
            ->condition('field_prize_country', $country)
            ->condition('field_prize_date.value', $this->getFormattedDate(), '<=')
            ->sort('field_prize_date', 'ASC')
            ->range(0, 1)
            ->execute();

        if (!empty($results)) {
            return $nodeStorage->load(array_shift(array_values($results)));
        }
    }

    /**
     * Get a formatted date to use in database queries. If no $date is passed, will return today's date
     */
    protected function getFormattedDate($date = null)
    {
        $newDate = new DrupalDateTime($date);

        $newDate->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));

        return $newDate->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
    }
}
