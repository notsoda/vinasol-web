<?php

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\Core\Site\Settings;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

function vinasol_theme_preprocess(&$variables, $hook)
{
    $variables['base_path'] = base_path();
}

/**
 * Prepares variables for HTML document templates.
 * @param $variables
 */
function vinasol_theme_preprocess_html(&$variables) {    
    $variables['environment'] = Settings::get('environment', 'dev');

    // if page is a node
    if (($node = \Drupal::routeMatch()->getParameter('node')) && $node instanceof NodeInterface) {
        // if the node's css-class field is not empty
        if ($node->hasField('field_css_class')) {
            // pass the field value to the tpl, where we will add it to the body_classes
            $variables['css_class'] = $node->get('field_css_class')->value;
        }
    }

    $routeName = \Drupal::routeMatch()->getRouteName();
    if (in_array($routeName, ['system.401', 'system.403', 'system.404'])) {
        $variables['attributes']['class'][] = 'errorPage';
    }
}

/**
 * Prepares variables for node templates.
 * @param $variables
 */
function vinasol_theme_preprocess_node(&$variables) {
    $request = \Drupal::request();
    $variables['currentPath'] = $request->getSchemeAndHttpHost() . $request->getRequestUri();

    if ($variables['view_mode'] !== 'full') {
        return;
    }

    // --- Node detail ---
    $session = \Drupal::request()->getSession();

    $variables['isPrizePage'] = false;

    // --- Contest Prize node page ---
    if ($variables['node']->id() != 15) {
        return;
    }
    
    $variables['isPrizePage'] = true;

    if (! $session->has('prize_id')) {
        $response = new RedirectResponse(Url::fromRoute('<front>')->toString());
        
        $response->send();

        return;
    }

    $prize = Node::load($session->get('prize_id'));

    if ($prize->getType() !== 'prize') {
        return;
    }

    $variables['prizeTitle'] = $prize->get('title')->value;

    $variables['prizeCategory'] = ucfirst($prize->get('field_prize_category')->value);

    // delete the prize session data
    $session->remove('prize_id');

    return;
}

/**
 * Prepares variables for block templates.
 * @param $variables
 */
function vinasol_theme_preprocess_block(&$variables) {
    // pass the bundle/type of block to its tpl, so we can use it as a css class, if any
    $variables['bundle'] = 'none';
    if (isset($variables['content']['#block_content'])){
        $variables['bundle'] = $variables['content']['#block_content']->bundle();
    }

    // block control depending on block id
    switch ($variables['attributes']['id']) {
        case 'block-sitebranding': // Site branding block
            // disable block caching
            $variables['#cache']['max-age'] = 0;
            break;
    }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form templates.
 * @param array $suggestions
 * @param array $variables
 */
function vinasol_theme_theme_suggestions_block_alter(array &$suggestions, array $variables) {
    // Block suggestions for custom block bundles.
    if (isset($variables['elements']['content']['#block_content'])) {
        array_splice($suggestions, 1, 0, 'block__bundle__' . $variables['elements']['content']['#block_content']->bundle());
    }
}

/**
 * Language switcher block override
 *
 * @param $variables
 */
function vinasol_theme_preprocess_links__language_block(&$variables) {
    $languageCode = \Drupal::languageManager()->getCurrentLanguage()->getId();

    foreach ($variables['links'] as $key => $link) {
        if ($key === $languageCode) {
            // add active class if is current language
            $variables['links'][$key]['link']['#options']['attributes']['class'][] = 'is-active';
        }
        
        // show just the language code
        $variables['links'][$key]['link']['#title'] = $key;
    }
}