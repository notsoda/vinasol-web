/**
 * Main script file
 */
const locale = {
    LEGAL_AGE_TITLE: {
        'es': "¡Qué ganas de celebrar!<br>Por favor, dinos tu fecha de nacimiento:",
        'ca': "Quines ganes de celebrar!<br>Per favor, digues la teva data de naixement:",
        'en': "It's time to celebrate!<br>Please, enter your birthdate:",
        'de': "Lasst uns feiern!<br>Bitte teilen Sie uns Ihr Geburtsdatum mit:"
    },
    LEGAL_AGE_DAY: {
        'es': "Día",
        'ca': "Dia",
        'en': "Day",
        'de': "Tag"
    },
    LEGAL_AGE_MONTH: {
        'es': "Mes",
        'ca': "Mes",
        'en': "Month",
        'de': "Monat"
    },
    LEGAL_AGE_YEAR: {
        'es': "Año",
        'ca': "Any",
        'en': "Year",
        'de': "Jahr"
    },
    LEGAL_AGE_SUBMIT: {
        'es': "Enviar",
        'ca': "Envia",
        'en': "Submit",
        'de': "Senden"
    },
    LEGAL_AGE_WRONG_DATA_MESSAGE: {
        'es': "Esta fecha no es válida.",
        'ca': "Aquesta data no és vàlida.",
        'en': "This is not a valid date.",
        'de': "Dieses Datum ist ungültig."
    },
    LEGAL_AGE_NON_ADULT_MESSAGE: {
        'es': "Lo sentimos, debes ser mayor de edad para acceder a esta página.",
        'ca': "Ho sentim, has de ser major d'edat per a accedir a aquesta pàgina.",
        'en': "We're sorry, you must be of legal drinking age to enter this site.",
        'de': "Leider müssen Sie volljährig sein, um auf diese Seite zugreifen zu können."
    },
    CONTEST_COUNTRY_SELECT_PAGE_PATH: {
        'es': '/concurso',
        'ca': '/ca/concurs',
        'en': '/en/contest',
        'de': '/de/wettbewerb',
        'fr': '/fr/concours',
        'nl': '/nl/wedstrijd'
    },
    CONTEST_ACTIVE_DATES: {
        'es': '15/06/2024 - 15/09/2024',
        'de': '01/05/2024 - 31/08/2024'
    },
    CONTEST_LANDING_PAGE_PATH: {
        'es': '/concurso-landing',
        'ca': '/ca/concurs-landing',
        'en': '/en/contest-landing',
        'de': '/de/wettbewerb-landing',
        'fr': '/fr/concours-landing',
        'nl': '/nl/wedstrijd-landing'
    },
    CONTEST_QUIZ_PAGE_PATH: {
        'en': '/en/contest-quiz'
    },
    CONTEST_FORM_PAGE_PATH: {
        'es': '/sube-tu-ticket',
        'ca': '/ca/puja-el-teu-ticket',
        'en': '/en/upload-your-receipt',
        'de': '/de/quittung-hochladen',
        'fr': '/fr/telecharger-le-recu',
        'nl': '/nl/upload-je-ontvangst'
    },
    UNAVAILABLE_PAGE_PATH: {
        'es': '/unavailable'
    },
    PLAY_VIDEO_CTA: {
        'es': "Ver spot",
        'ca': "Veure spot",
        'en': "Play video"
    }
};
let config = {
    LANGUAGE: 'es',
    BROWSER_LANGUAGE: 'en',
    INTRO_PAUSE_TIME: 500, // miliseconds
    INTRO_ANIM_TIME: 1500, // miliseconds
    MODAL_PLAYER: null,
    IP_API_ENDPOINT: 'https://pro.ip-api.com/json?key=XAgOvdLEDwKHd17&fields=countryCode,region'
};

(function ($, window, document, undefined) {
    // ON READY
    $(window).ready(function(){
        config.LANGUAGE = $('html').attr('lang');
        config.BROWSER_LANGUAGE = getBrowserLanguage();

        // check user country
        checkCountry();

        // For all links with rel external, open link in new tab
        $('body').on('click', 'a[rel="external"]', function(e){
            e.preventDefault();
            window.open($(this).attr('href'));
        });

        // has Legal Age modal cookie ?
        if (! getCookie('vinasol_legal_age')) {
            prepareLegalAgeModal();
        } else {
            prepareHomeHeroCampaignVideoIntro();
        }

        // modal menu
        prepareModalMenu();

        // messages
        var $allMessages = $('.allMessages');
        if ($allMessages.length) {
            // show
            $allMessages.addClass('shown');

            // for each of its messages
            $allMessages.find('.messages').each(function(i){
                $(this).find('button.close').click(function(e){
                    $(this).parent().fadeOut(300, function() { $(this).remove(); });
                });
            });
        }

        // fixed header: must be the normal (white background) header
        // which is done through css
        $('body').addClass('notScrolled');
        checkIfWindowIsScrolled();

        // our wines menu
        prepareOurWinesMenu();

        // open hero video as overlay
        $.modal.defaults = {
            escapeClose: true,
            clickClose: true,
            closeText: '<svg width="24" height="23" viewBox="0 0 24 23" fill="none">' +
            '<line y1="-0.75" x2="30.8049" y2="-0.75" transform="matrix(0.714172 0.69997 -0.514309 0.857605 1 1.4375)" stroke="#FFF" stroke-width="1.5"/>' +
            '<line y1="-0.75" x2="30.5129" y2="-0.75" transform="matrix(0.721008 -0.692927 0.506896 0.862007 1 22.5807)" stroke="#FFF" stroke-width="1.5"/>' +
            '</svg>',
            showClose: true,                 
            fadeDuration: 200
        };
        prepareHeroVideo();

        // animate title appearance
        if (window.matchMedia('(min-width: 701px)').matches) {
            prepareTitleAppearance();
        }

        // animate image appearance
        prepareImageAppearance();

        // wines list
        if (window.matchMedia('(min-width: 801px)').matches) {
            // animate wines appearance
            prepareWinesAppearance();
        } else {
            // if screen width <= 800px, make the wines list a slider
            $('.wines-list .layout__region--content').slick({
                arrows: false,
                dots: true,
                infinite: false
            });
        }

        // History milestones
        prepareHistory();
        
        // Contest countries
        prepareContestCountries();
        // Contest pages
        prepareContest();

        // wine card zoom/lens effect
        $('.wine-card .paragraph--type--image').find('.field--name-field-image, .field--name-field-image-alt').find('article .field--name-field-media-image').each(function(i){
            const $pane = $(this).parent().parent().parent().parent().siblings().first();
            $pane.addClass('magnifiedPane');
            new Drift($(this).find('img').get(0), {
                namespace: 'magnify-',
                sourceAttribute: 'src',
                paneContainer: $pane.get(0),
                inlinePane: 1080,
                zoomFactor: 2,
                onShow: () => {
                    $pane.addClass('magnifiedPaneVisible');
                },
                onHide: () => {
                    $pane.removeClass('magnifiedPaneVisible');
                },
            });
        });

        // translate Legal menu's whistleblowing link url
        $('#block-legal-menu a[href="https://report.whistleb.com/es/torres"]').attr('href', 'https://report.whistleb.com/' + config.LANGUAGE + '/torres');
        
    });

    // ON SCROLL
    $(window).scroll(function() {
        checkIfWindowIsScrolled();
    });

    // ON MODAL BEFORE OPEN
    $(window).on($.modal.BEFORE_OPEN, function(e, m) {
        config.MODAL_PLAYER = new Plyr(m.$elm, {
            autoplay: true,
            loop: { active: false },
            muted: false,
            controls: true,
            clickToPlay: true,
            disableContextMenu: false,
            storage: { enabled: false }
        });
    });

    // ON MODAL BEFORE CLOSE
    $(window).on($.modal.BEFORE_CLOSE, function(e, m) {
        config.MODAL_PLAYER.destroy();
    });

    function checkCountry() {
        let countryCode = getCookie('vinasol_country');
        let region = getCookie('vinasol_region');

        if (! countryCode || ! region) {
            $.ajax({
                url: config.IP_API_ENDPOINT,
            }).done(data => {
                countryCode = data['countryCode'].toUpperCase();
                setCookie('vinasol_country', countryCode, 30);

                region = data['region'].toUpperCase();
                setCookie('vinasol_region', region, 30);

                checkSomeCountryCodes(countryCode, region);
            });

            return;
        }

        checkSomeCountryCodes(countryCode, region);
    }

    function checkSomeCountryCodes(countryCode, region) {
        if (countryCode === 'NO') {
            const path = location.pathname;
            if (path !== locale.UNAVAILABLE_PAGE_PATH[config.LANGUAGE]) {
                location.href = locale.UNAVAILABLE_PAGE_PATH[config.LANGUAGE];
            }
            return;
        }

        if (checkIfFromUKOrIreland(countryCode) || checkIfFromCanaryIslands(countryCode, region)) {
            // show alternate wine image if any
            $('.paragraph--type--product-teaser, .wine-card .paragraph--type--image').each(function(i) {
                const $altImage = $(this).find('.field--name-field-image-alt');
                if ($altImage.length) {
                    $(this).find('.field--name-field-image').hide();
                    $altImage.show();
                }
            });

            // hide the organic wine section
            $('#organic-wine').hide();
            return;
        }
    }

    function checkIfFromUKOrIreland(countryCode) {
        return ['GB', 'IE'].includes(countryCode);
    }

    function checkIfFromCanaryIslands(countryCode, region) {
        return (countryCode === 'ES' && region == 'IC');
    }

    function prepareHistory() {
        const $history = $('#about-history');
        if ($history.length) {
            if (window.matchMedia('(min-width: 901px)').matches) {
                // move all the item's wine bottles to a sibling div
                $history.find('> .layout').prepend('<div class="wineBottles"></div>');
                const $wineBottles = $history.find('.wineBottles');
                $history.find('.field--name-field-milestone-wine-img').appendTo($wineBottles);
                $wineBottles.find('> div').eq(0).addClass('current');
                // create the slider
                $history.find('> .layout > .layout__region').addClass('swiper-wrapper');
                $history.find('.swiper-wrapper > div').addClass('swiper-slide');
                const swiper = new Swiper('#about-history > .layout', {
                    direction: 'vertical',
                    mousewheel: {
                        forceToAxis: true,
                        sensitivity: .75
                    },
                    on: {
                        slideChangeTransitionStart: (swiper) => { // on slide change:
                            // mark the related bottle as current
                            $wineBottles.find('> div').each(function(i){
                                if (i === swiper.activeIndex) {
                                    $(this).addClass('current');
                                } else {
                                    $(this).removeClass('current');
                                }
                            });
                        }
                    }
                });
            } else {
                // if screen width <= 900px, make a slick slider
                $history.find('> .layout > .layout__region').slick({
                    arrows: false,
                    dots: true,
                    vertical: true,
                    verticalSwiping: true,
                    infinite: false,
                    adaptiveHeight: true
                });
            }
        }
    }

    function prepareLegalAgeModal() {
        $('body').addClass('hasLegalModal').append('<div class="modal legalAgeModal" id="legal-age-modal">' +
        '<div class="inner">' +
            '<div class="logo"></div>' +
            '<h1>'+ locale.LEGAL_AGE_TITLE[config.BROWSER_LANGUAGE] +'</h1>' +
            '<div class="birthdateSelector">' +
                '<input type="text" name="day" placeholder="'+ locale.LEGAL_AGE_DAY[config.BROWSER_LANGUAGE] +'">' +
                '<input type="text" name="month" placeholder="'+ locale.LEGAL_AGE_MONTH[config.BROWSER_LANGUAGE] +'">' +
                '<input type="text" name="year" placeholder="'+ locale.LEGAL_AGE_YEAR[config.BROWSER_LANGUAGE] +'">' +
                '<button data-action="submit">'+ locale.LEGAL_AGE_SUBMIT[config.BROWSER_LANGUAGE] +'</button>' +
            '</div>' +
            '<p class="errorMessage" />' +
        '</div>' +
        '</div>');
        const showMessage = (message) => {
            $('.legalAgeModal').find('.errorMessage').text(message);
        };
        const checkAge = () => {
            const today = new Date();
            let inputValues = [];
            $('.legalAgeModal').find('.birthdateSelector input[type="text"]').each(function(i){
                const value = parseInt($(this).val());
                if (! isNaN(value)) {
                    inputValues.push(value);
                }
            });
            if (inputValues.length < 3 || inputValues[2] < 1920 || inputValues[2] >= today.getFullYear()) {
                showMessage(locale.LEGAL_AGE_WRONG_DATA_MESSAGE[config.BROWSER_LANGUAGE]);
                return;
            };
            const birthdate = new Date(inputValues[2], inputValues[1] - 1, inputValues[0]);
            const diffInYears = Math.floor(((today.getTime() - birthdate.getTime()) / (1000 * 60 * 60 * 24 * 365.25)));
            if (diffInYears >= 18) {
                setCookie('vinasol_legal_age', '1', 120);
                $.modal.close();
                $('body').removeClass('hasLegalModal');
                prepareHomeHeroCampaignVideoIntro();
                return;
            }
            // not an adult
            showMessage(locale.LEGAL_AGE_NON_ADULT_MESSAGE[config.BROWSER_LANGUAGE]);
        };
        $('.legalAgeModal [data-action="submit"]').click(function(e){
            e.preventDefault();
            checkAge();
        });
        $('#legal-age-modal').modal({
            escapeClose: false,
            clickClose: false,
            showClose: false
        });
    }

    function checkIfWindowIsScrolled() {
        if ($(window).scrollTop() >= 1) {
            $('body').removeClass('notScrolled');
        } else {
            $('body').addClass('notScrolled');
        }
    }

    function prepareModalMenu() {
        $('.layout-container').prepend('<div class="modalMenu">' +
        '<button class="openModalMenu"><svg width="36" height="20" viewBox="0 0 36 20" fill="none">' +
        '<line y1="1.25" x2="35.1177" y2="1.25" stroke="#FAF9F0" stroke-width="1.5"/>' +
        '<line y1="10.2501" x2="35.1177" y2="10.2501" stroke="#FAF9F0" stroke-width="1.5"/>' +
        '<line y1="19.25" x2="35.1177" y2="19.25" stroke="#FAF9F0" stroke-width="1.5"/>' +
        '</svg></button>' +
        '<button class="closeModalMenu"><svg width="24" height="23" viewBox="0 0 24 23" fill="none">' +
        '<line y1="-0.75" x2="30.8049" y2="-0.75" transform="matrix(0.714172 0.69997 -0.514309 0.857605 1 1.4375)" stroke="#FAF9F0" stroke-width="1.5"/>' +
        '<line y1="-0.75" x2="30.5129" y2="-0.75" transform="matrix(0.721008 -0.692927 0.506896 0.862007 1 22.5807)" stroke="#FAF9F0" stroke-width="1.5"/>' +
        '</svg></button>' +
        '<div class="inner"></div></div>');
        var $menuInner = $('.modalMenu .inner');
        $menuInner.append($('.region-header > .inner').html());
        $menuInner.find('.block-system-branding-block svg').remove(); // remove the logo svg as we would have problems with the solar gradient dissappearing as the html would have two same ids (the gradient)
        var toggleMenu = function(){
            $('body').toggleClass('modalMenuIsOpen');
        };
        $('.openModalMenu').click(toggleMenu);
        $('.closeModalMenu').click(toggleMenu);
    }

    function prepareOurWinesMenu() {
        var $menu = $('#our-wines-menu');
        if ($menu.length) {
            $menu.find('.wines-list .paragraph--type--product-teaser').each(function(i){
                // for each wine:
                // create a link to the wine's CTA link
                $(this).find('.field--name-field-image, .field--name-field-image-alt').find('img').wrap('<a href="' + $(this).find('.field--name-field-cta a').attr('href') + '"></a>');
                // on hover change css classes
                $(this).hover(
                    function() {
                        $menu.addClass('menuHovered');
                        $(this).addClass('itemHovered');
                    }, function() {
                        $menu.removeClass('menuHovered');
                        $(this).removeClass('itemHovered');
                    }
                );
            });
        }
    }

    function prepareHomeHeroCampaignVideoIntro() {
        // in the homepage and with a campaign video hero
        if ($('body').hasClass('path-frontpage') && $('#hero-campaign-video').length) {
            // has the intro cookie ?
            if (! getCookie('vinasol_intro')) {
                $('body').attr('data-intro-step', 1).addClass('campaignVideoIntro');
                setTimeout(() => {
                    $('body').attr('data-intro-step', 2);
                }, config.INTRO_PAUSE_TIME);
                setTimeout(() => {
                    $('body').attr('data-intro-step', 3);
                }, config.INTRO_PAUSE_TIME + config.INTRO_ANIM_TIME);
                setTimeout(() => {
                    $('body').removeAttr('data-intro-step').removeClass('campaignVideoIntro');
                    setCookie('vinasol_intro', '1', 30);
                }, config.INTRO_PAUSE_TIME + config.INTRO_ANIM_TIME + 500);
            }
        }
    }

    function prepareHeroVideo() {
        $('.paragraph--type--hero').each(function(i){
            const $hero = $(this);
            // prepare the full video in a modal
            const $heroEmbedFull = $hero.find('.field--name-field-video-embed');
            if ($heroEmbedFull.length) {
                const iframeHtml = $heroEmbedFull.html();
                $heroEmbedFull.after('<div class="modal videoModal" id="hero-video-modal">' + iframeHtml + '</div>');
                const playButtonHtml = '<a href="#hero-video-modal" rel="modal:open" class="btnPlay">' +
                '<svg width="14" height="16" viewBox="0 0 14 16" fill="none">' +
                '<path d="M13.5 7.13397C14.1667 7.51887 14.1667 8.48112 13.5 8.86602L1.5 15.7942C0.833333 16.1791 -7.73604e-07 15.698 -7.39955e-07 14.9282L-1.34273e-07 1.0718C-1.00623e-07 0.301996 0.833333 -0.17913 1.5 0.20577L13.5 7.13397Z" fill="#0A0A0A"/>' +
                '</svg>'+locale.PLAY_VIDEO_CTA[config.LANGUAGE]+'</a>';
                $heroEmbedFull.parent().find('.field--name-field-body').after(playButtonHtml); 
            }
        });
    }

    function prepareTitleAppearance() {
        const $pageTitles = $('article.node--view-mode-full').find('h1, h2, h3');
        if ($pageTitles.length) {
            $pageTitles.each(function(){
                const $title = $(this);
                $title.addClass('appearOnScroll');
                // wrap each new line (br) inside span tags
                let htmlText = $title.html();
                const openingTag = '<span>';
                const closingTag = '</span>';
                htmlText = openingTag + htmlText.replace('<br>', closingTag + openingTag) + closingTag;
                $title.html(htmlText);
                // add animation classes for each line
                const delayIncrease = .25;
                let delay = .25;
                $title.find('span').each(function(){
                    $(this).addClass('animate__animated animate__fadeInUp');
                    delay += delayIncrease;
                    $(this)[0].style.setProperty('--animate-duration', delay + 's');
                });
            });
            const observer = new IntersectionObserver(entries => {
                entries.forEach(entry => {
                    if (entry.isIntersecting) {
                        const $title = $(entry.target);
                        if (! $title.hasClass('appearOnScrollStarted')) {
                            $title.addClass('appearOnScrollStarted');
                        }
                    }
                });
            }, {
                rootMargin: '0px 0px -200px 0px'
            });
            $pageTitles.each(function(){
                observer.observe($(this)[0]);
            });
        }
    }

    function prepareImageAppearance() {
        // single image
        let $images = $('.paragraph--type--image').find('.field--name-field-image, .field--name-field-image-alt');
        if ($images.length) {
            $images.each(function(){
                const $image = $(this);
                $image.addClass('appearOnScroll');
                $image.find('article.media').each(function(){
                    $(this).addClass('animate__animated animate__fadeInUp');
                });
            });
            const observer = new IntersectionObserver(entries => {
                entries.forEach(entry => {
                    if (entry.isIntersecting) {
                        const $image = $(entry.target);
                        if (! $image.hasClass('appearOnScrollStarted')) {
                            $image.addClass('appearOnScrollStarted');
                        }
                    }
                });
            }, {
                rootMargin: '0px 0px -100px 0px'
            });
            $images.each(function(){
                observer.observe($(this)[0]);
            });
        }

        // multiple images
        $images = $('.field--name-field-images');
        if ($images.length) {
            $images.each(function(){
                const $image = $(this);
                $image.addClass('appearOnScroll');
                const delayIncrease = .25;
                let delay = .25;
                $image.find('.field__item').each(function(){
                    $(this).addClass('animate__animated animate__fadeInUp');
                    delay += delayIncrease;
                    $(this)[0].style.setProperty('--animate-duration', delay + 's');
                });
            });
            const observer = new IntersectionObserver(entries => {
                entries.forEach(entry => {
                    if (entry.isIntersecting) {
                        const $image = $(entry.target);
                        if (! $image.hasClass('appearOnScrollStarted')) {
                            $image.addClass('appearOnScrollStarted');
                        }
                    }
                });
            }, {
                rootMargin: '0px 0px -100px 0px'
            });
            $images.each(function(){
                observer.observe($(this)[0]);
            });
        }
    }

    function prepareWinesAppearance() {
        const $wineLists = $('.wines-list');
        if ($wineLists.length) {
            $wineLists.each(function(){
                const $wineList = $(this);
                $wineList.addClass('appearOnScroll');
                // add animation classes for each wine
                const delayIncrease = .40;
                let delay = .40;
                $wineList.find('.paragraph--type--product-teaser').each(function(){
                    $(this).addClass('animate__animated animate__fadeInUp');
                    delay += delayIncrease;
                    $(this)[0].style.setProperty('--animate-duration', delay + 's');
                });
            });
            const observer = new IntersectionObserver(entries => {
                entries.forEach(entry => {
                    if (entry.isIntersecting) {
                        const $wineList = $(entry.target);
                        if (! $wineList.hasClass('appearOnScrollStarted')) {
                            $wineList.addClass('appearOnScrollStarted');
                        }
                    }
                });
            }, {
                rootMargin: '0px 0px -200px 0px'
            });
            $wineLists.each(function(){
                observer.observe($(this)[0]);
            });
        }
    }

    function prepareContest() {
        const path = location.pathname;
        const $contestPage = $('body.contest-page');
        if ($contestPage.length) {
            // close button
            $contestPage.find('.region-content').prepend('<button class="contestPageClose"><svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">' +
                '<path d="M17.5303 2.53033L18.0607 2L17 0.93934L16.4697 1.46967L17.5303 2.53033ZM1.46967 16.4697L0.93934 17L2 18.0607L2.53033 17.5303L1.46967 16.4697ZM16.4697 1.46967L1.46967 16.4697L2.53033 17.5303L17.5303 2.53033L16.4697 1.46967Z" fill="#0A0A0A"/>' +
                '<path d="M16.4697 17.5303L17 18.0607L18.0607 17L17.5303 16.4697L16.4697 17.5303ZM2.53033 1.46967L2 0.93934L0.93934 2L1.46967 2.53033L2.53033 1.46967ZM17.5303 16.4697L2.53033 1.46967L1.46967 2.53033L16.4697 17.5303L17.5303 16.4697Z" fill="#0A0A0A"/>' +
                '</svg></button>');
            $contestPage.find('button.contestPageClose').click(function(e){
                e.preventDefault();
                location.href = locale.CONTEST_COUNTRY_SELECT_PAGE_PATH[config.LANGUAGE];
            });

            switch (path) {
                case locale.CONTEST_QUIZ_PAGE_PATH[config.LANGUAGE]: // the quiz page
                    prepareContestQuiz();
                    break;
            }
        }
    }

    function prepareContestCountries() {
        const $countrySelector = $('#block-contest-countries');
        if ($countrySelector.length === 0) return;

        $countrySelector.appendTo('.contest-choose-country-hero .paragraph--type--hero .field--name-field-body');

        $countrySelector.find('.view-contest-countries .item-list li').each(function(i){
            const codePrefix = 'country-';
            let code = $(this).attr('class');
            code = code.substring(codePrefix.length);
            const enabledCountries = ['es', 'de'];
            const isDisabled = ! enabledCountries.includes(code);
            const $imageField = $(this).find('.views-field-field-image');
            const $nameField = $(this).find('.views-field-name');
            const label = 'country-' + code;
            const disabledAttr = isDisabled ? ' disabled' : '';

            if (isDisabled) $(this).addClass('disabled');

            $nameField.prepend('<input type="radio" id="' + label + '" name="country" '+disabledAttr+' value="' + code + '">');
            $imageField.find('img').wrap('<label for="'+label+'">');
            $nameField.find('.field-content').wrap('<label for="'+label+'">');
            $nameField.after('<div class="activeDates">' + locale.CONTEST_ACTIVE_DATES[code] + '</div>');

            const $radioButtons = $nameField.find('input[name="country"]');
            $radioButtons.change(function(ev) {
                ev.preventDefault();
                if (this.value === 'ie') { // only for Ireland
                    // go to the Quiz page
                    location.href = locale.CONTEST_QUIZ_PAGE_PATH['en'];
                    return;
                }

                // go to the landing page for the selected country
                location.href = locale.CONTEST_LANDING_PAGE_PATH[this.value];
            });
        });
    }

    function prepareContestQuiz() {
        const quizQuestions = [
            {
                title: "Where is Viña Sol from?",
                correct: 1,
                answers: [
                    "Ireland",
                    "The Mediterranean",
                    "France"
                ]
            },
            {
                title: "How is it made?",
                correct: 0,
                answers: [
                    "Solar energy and other renewable energy sources",
                    "Thermal energy",
                    "Wind energy"
                ]
            },
            {
                title: "What's the prize?",
                correct: 2,
                answers: [
                    "A pair of sunglasses",
                    "A corkscrew",
                    "A stay in a Mediterranean villa"
                ]
            }
        ];

        $('article.node--type-page .paragraph--type--text .field--name-field-body').after('<div class="contestQuiz"><div class="quizMessage"></div><div class="quizQuestions"></div></div>');
        const $quiz = $('.contestQuiz');
        const $questions = $quiz.find('.quizQuestions');
        quizQuestions.forEach((question, questionIndex) => {
            let questionHTML = '<div class="quizQuestion"><h3>' + question.title + '</h3>';
            questionHTML += '<div class="quizAnswers">';
            question.answers.forEach((answer, answerIndex) => {
                const questionLabel = 'question-' + questionIndex;
                const answerLabel = questionLabel + '-' + answerIndex;
                questionHTML += '<div class="quizAnswer">';
                questionHTML += '<input type="radio" name="' + questionLabel + '" value="'+answerIndex+'" id="' + answerLabel + '"> <label for="' + answerLabel + '">' + answer + '</label>';
                questionHTML += '</div>';
            });
            questionHTML += '</div></div>';
            $questions.append(questionHTML);
        });

        const showMessage = (message) => {
            $quiz.find('.quizMessage').text(message);
        };

        const checkAnswers = () => {
            let correctCount = 0;
            showMessage('');
            quizQuestions.forEach((question, questionIndex) => {
                const questionLabel = 'question-' + questionIndex;
                $questions.find('input[name="'+questionLabel+'"]').each(function(i){
                    if ($(this).is(':checked') && question.correct == this.value) {
                        correctCount++;
                    }
                });
            });

            $questions.find('input[type="radio"]').prop('checked', false);

            if (correctCount < quizQuestions.length) {
                showMessage("Ooops! Wrong answer. Try it again!");
                return;
            }

            location.href = locale.CONTEST_FORM_PAGE_PATH[config.LANGUAGE] + '?country=ie';
        };

        $quiz.append('<button data-action="submitQuiz">Next</button>');
        const $quizButton = $quiz.find('button');
        $quizButton.click(function(ev){
            ev.preventDefault();
            checkAnswers();
        });
    }

})(jQuery, this, this.document);

function getBrowserLanguage() {
    const browserLanguage = navigator.language;
    const browserLanguageParts = browserLanguage.split('-');
    let finalLanguage = 'en';
    const availableLanguages = ['es', 'en', 'de', 'fr'];
    if (availableLanguages.includes(browserLanguageParts[0])) {
        finalLanguage = browserLanguageParts[0];
    }
    return finalLanguage;
}

/**
* Basic cookies functions
*/
function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return false;
}
